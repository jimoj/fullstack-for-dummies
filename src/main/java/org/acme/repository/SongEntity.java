package org.acme.repository;

import io.quarkus.mongodb.panache.PanacheMongoEntity;

public class SongEntity {

    private String name;
    private String author;
   // private Double duration;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

   /* public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }*/
}
