package org.acme;

import org.acme.repository.SongEntity;
import org.acme.repository.SongsRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/songs")
public class SpringGreetingController {

    @Inject
    private SongsRepository songsRepository;

   @GetMapping
    public String hello() {
        return "Hello World";
    }

    @GetMapping(value = "/create")
    public void createdb () {
        SongEntity songEntity = new SongEntity();
        songEntity.setAuthor("Hayley williams");
        songEntity.setName("Simmer");
        songsRepository.persist(songEntity);
    }

    @PostMapping(value = "/addSong")
    public ResponseEntity<SongEntity> addSong (SongEntity song) {
        try{
            songsRepository.persistOrUpdate(song);
            return ResponseEntity.ok(song);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
