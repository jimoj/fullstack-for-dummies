package org.acme.repository;

import io.quarkus.mongodb.panache.PanacheMongoRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SongsRepository implements PanacheMongoRepository<SongEntity> {
}
